<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'id' => 1,
            'label' => 'Admin',
            'machine_name' => 'admin'
        ]);
        Role::create([
            'id' => 2,
            'label' => 'Seller',
            'machine_name' => 'seller'
        ]);
    }
}
