<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'firstname' => 'Soufian',
            'lastname' => 'AIT TIRITE',
            'displayname' => 'Soufian AIT TIRITE',
            'email' => 'contact@soufian-a.net',
            'password' => Hash::make('monmotdepasse'),
            'role_id' => 1
        ]);
    }
}
