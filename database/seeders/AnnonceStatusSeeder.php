<?php

namespace Database\Seeders;

use App\Models\AnnonceStatus;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AnnonceStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AnnonceStatus::create([
            "label" => "Brouillon",
            "display_public" => false,
        ]);
        AnnonceStatus::create([
            "label" => "Publié",
            "display_public" => true,
        ]);
        AnnonceStatus::create([
            "label" => "Rejeté par le modérateur",
            "display_public" => false,
        ]);
    }
}
