<?php

use App\Http\Controllers\UserController;
use App\Http\Controllers\AnnonceController;
use App\Http\Controllers\AnnonceStatusController;
use App\Http\Controllers\CommentsController;
use App\Http\Controllers\LoginController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::prefix('login')->group(function() {
    Route::get('/', [LoginController::class, 'login'])->name('login');
    Route::post('/', [LoginController::class, 'authenticate'])->name('login.attempt');
});

Route::get('/logout', [LoginController::class, 'logout'])->name('logout');

Route::prefix('user')->group(function () {
    Route::get('/', [UserController::class, 'index'])->name('user.index');
    Route::get('/{id}', [UserController::class, 'show'])->name('user.show');
    Route::get('/{id}/update', [UserController::class, 'update'])->name('user.update');
    Route::put('/{id}', [UserController::class, 'edit'])->name('user.edit');
    Route::get('/create', [UserController::class, 'create'])->name('user.create');
    Route::post('/', [UserController::class, 'store'])->name('user.store');
    Route::delete('/{id}', [UserController::class, 'destroy'])->name('user.destroy');
});

Route::prefix('annonce')->group(function () {
    Route::middleware(['auth','role:admin'])->prefix('status')->group(function () {
        Route::get('/', [AnnonceStatusController::class, 'index'])->name('annonce_status.index');
        Route::get('/{id}', [AnnonceStatusController::class, 'show'])->name('annonce_status.show');
        Route::get('/{id}/update', [AnnonceStatusController::class, 'update'])->name('annonce_status.update');
        Route::put('/{id}', [AnnonceStatusController::class, 'edit'])->name('annonce_status.edit');
        Route::get('/create', [AnnonceStatusController::class, 'create'])->name('annonce_status.create');
        Route::post('/', [AnnonceStatusController::class, 'store'])->name('annonce_status.store');
        Route::delete('/{id}', [AnnonceStatusController::class, 'destroy'])->name('annonce_status.destroy');
    });
    Route::get('/', [AnnonceController::class, 'index'])->name('annonce.index');
    Route::middleware(['auth','role:seller:admin'])->group(function() {
        Route::get('/{annonce}/update', [AnnonceController::class, 'update'])->name('annonce.update');
        Route::put('/{id}', [AnnonceController::class, 'edit'])->name('annonce.edit');
        Route::get('/create', [AnnonceController::class, 'create'])->name('annonce.create');
        Route::post('/', [AnnonceController::class, 'store'])->name('annonce.store');
        Route::delete('/{id}', [AnnonceController::class, 'destroy'])->name('annonce.destroy');
    });
    // using Service Container to Inject Annonce dependency
    Route::get('/{annonce}', [AnnonceController::class, 'show'])->name('annonce.show');
});

Route::prefix('comments')->group(function () {
    Route::put('/{id}', [CommentsController::class, 'edit'])->name('comments.edit');
    Route::post('/', [CommentsController::class, 'store'])->name('comments.store');
    Route::delete('/{id}', [CommentsController::class, 'destroy'])->name('comments.destroy');
});

