<?php

use App\Http\Controllers\Api\AnnonceApiController;
use App\Http\Controllers\Api\SanctumController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/sanctum/token', [SanctumController::class, 'authenticate'])->name('sanctum.authenticate');

Route::middleware(['auth:sanctum','role'])->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware(['auth:sanctum','role'])->prefix('annonce')->group(function() {
    Route::get('/', [AnnonceApiController::class, 'index'])->name('api.annonce.index');
    Route::get('/{id}', [AnnonceApiController::class, 'get'])->name('api.annonce.get');
    Route::put('/{id}/publish', [AnnonceApiController::class, 'publish'])->name('api.annonce.publish');
    Route::put('/{id}', [AnnonceApiController::class, 'update'])->name('api.annonce.update');
    Route::post('/', [AnnonceApiController::class, 'create'])->name('api.annonce.create');
    Route::delete('/{id}', [AnnonceApiController::class, 'delete'])->name('api.annonce.delete');
});
