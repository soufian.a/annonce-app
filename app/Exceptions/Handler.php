<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use PhpParser\Node\Expr\Throw_;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<\Throwable>, \Psr\Log\LogLevel::*>
     */
    protected $levels = [
        //
    ];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<\Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    /**
     * Render sorted by Exceptions types.
     *
     * @return void
     */
    public function render($request, Throwable $e)
    {
        if ($e instanceof \Illuminate\Validation\ValidationException) {
            if($request->isJson())
                return response()->json([ 'message' => 'Les données envoyés ne respectent le format attendu.'], 403);
            $failedFIelds = array_keys($e->validator->failed());
            $errors = [];
            foreach ($failedFIelds as $value) {
                $errors[$value] = 'Les données envoyés ne respectent le format attendu.';
            }
            return back()->withErrors($errors);
        }
        return parent::render($request,$e);
    }
}
