<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class SanctumController extends Controller
{
    /** Implement authentication for API by email, password and device_name
     *
     * @param \illuminate\Http\Request $request
     * @return \illuminate\Http\Response
     */
    public function authenticate(Request $request)
    {
        // try {
            $validation = $request->validate([
                'email' => 'required|email',
                'password' => 'required',
                'device_name' => 'required'
            ]);

            if (!$validation) {
                return response()->json([
                    'message' => 'error on validation fields : email, password and device_name are required'
                ], 403);
            }

            $user = User::where('email', $request->email)->first();

            if (!$user || !Hash::check($request->password, $user->password)) {
                throw ValidationException::withMessages([
                    'email' => ['The provided credentials are incorrect.'],
                ]);
            }
        // } catch (\Exception $e) {
        //     dd(get_class($e));
        // }

        return response()->json([
            'token' => $user->createToken($request->device_name)->plainTextToken
        ]);
    }
}
