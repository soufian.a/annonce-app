<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Annonce;
use App\Models\AnnonceStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AnnonceApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Annonce::whereRelation('status', 'display_public', true)->get());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function get($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function publish($id) {
        $annonce = Annonce::findOrfail($id);
        $annonce->status_id = AnnonceStatus::where('display_public', true)->first()->id;
        $annonce->save();

        return response()->json($annonce, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validation = $request->validate([
            "title" => [ "required" ],
            "description" => [ "required" ],
            "price" => [ "required", "numeric" ],
            "image" => [ "required" ]
        ]);

        if(!$validation) {
            return response()->json([
                "message" => "Une erreur de validation à lieu, merci de vérifier les données envoyés"
            ], 500);
        }

        $annonce = new Annonce();
        $annonce->title = $request->title;
        $annonce->description = $request->description;
        $annonce->price = $request->price;
        $annonce->image = $request->image;
        $annonce->contact_name = $request->contact_name;
        $annonce->contact_email = $request->contact_email;
        $annonce->contact_phone_number = $request->contact_phone_number;

        $annonce->user_id = Auth::user()->id;
        $annonce->status_id = AnnonceStatus::where('display_public', false)->first()->id;

        $annonce->save();

        return response()->json($annonce, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        //
    }
}
