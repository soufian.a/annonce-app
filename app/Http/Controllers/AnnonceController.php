<?php

namespace App\Http\Controllers;

use App\Models\Annonce;
use App\Models\AnnonceStatus;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AnnonceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $annonces = Annonce::whereRelation('status', 'display_public', true)->get();

        return view('annonce.index', ['annonces' => $annonces]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $annonce_status_list = AnnonceStatus::all();
        $annonce_statuses = [];
        foreach ($annonce_status_list as $annonce_status) {
            $annonce_statuses[$annonce_status->id] = $annonce_status->label;
        }
        return view('annonce.create', [ 'annonce_statuses' => $annonce_statuses ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "title" => [ "required" ],
            "description" => [ "required" ],
            "price" => [ "required", "numeric" ],
            // "image" => [ "required" ]
        ]);

        $annonce = new Annonce();
        $annonce->title = $request->title;
        $annonce->description = $request->description;
        $annonce->price = $request->price;
        $annonce->image = '/image.png';// $request->image;
        $annonce->contact_name = $request->contact_name;
        $annonce->contact_email = $request->contact_email;
        $annonce->contact_phone_number = $request->contact_phone_number;

        $annonce->user_id = Auth::user()->id;
        $annonce->status_id = $request->status_id;

        $annonce->save();

        return response()->redirectToRoute('annonce.show', $annonce->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Annonce  $annonce
     * @return \Illuminate\Http\Response
     */
    public function show(Annonce $annonce)
    {
        // $annonce = Annonce::find(); // NOT NEEDED ANYMORE WITH SERVICE CONTAINER DEPENDENCY INJECTION
        return view('annonce.show', [ 'annonce' => $annonce, 'user' => Auth::user() ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Annonce  $annonce
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Annonce $annonce)
    {
        $request->validate([
            "title" => [ "required" ],
            "description" => [ "required" ],
            "price" => [ "required", "numeric" ],
            // "image" => [ "required" ]
        ]);

        $annonce->title = $request->title;
        $annonce->description = $request->description;
        $annonce->price = $request->price;
        $annonce->image = '/image.png';// $request->image;
        $annonce->contact_name = $request->contact_name;
        $annonce->contact_email = $request->contact_email;
        $annonce->contact_phone_number = $request->contact_phone_number;
        $annonce->status_id = $request->status_id;

        $annonce->save();

        return response()->redirectToRoute('annonce.show', $annonce->id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Annonce  $annonce
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Annonce $annonce)
    {
        $annonce_status_list = AnnonceStatus::all();
        $annonce_statuses = [];
        foreach ($annonce_status_list as $annonce_status) {
            $annonce_statuses[$annonce_status->id] = $annonce_status->label;
        }
        return view('annonce.update', [ 'annonce' => $annonce, 'annonce_statuses' => $annonce_statuses ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
