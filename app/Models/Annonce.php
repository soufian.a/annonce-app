<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Annonce extends Model
{
    use HasFactory;

    public function comments()
    {
        return $this->hasMany(Comments::class);
    }

    public function status()
    {
        return $this->belongsTo(AnnonceStatus::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
