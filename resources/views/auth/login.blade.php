@extends('layout')

@section('page_title')
Connexion
@endsection

@section('content')
<h2>Connectez-vous avec vos identifiants</h2>
@error('email')
    <p class="alert alert-danger">{{ $message }}</p>
@enderror
@error('password')
    <p class="alert alert-danger">{{ $message }}</p>
@enderror
<form action="{{ route('login.attempt') }}" method="POST">
    @csrf
    <input type="email" placeholder="Email" name="email" value="{{ old('email') }}"/>
    <input type="password" placeholder="Mot de passe" name="password"/>
    <input type="submit" value="Connexion"/>
</form>
@endsection
