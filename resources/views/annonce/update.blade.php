@extends('layout')

@section('page_title')
Modifier mon annonce
@endsection

@section('content')
<h2>Modifier mon annonce</h2>

{{ Form::model($annonce, ['route' => ['annonce.edit', [ 'id' => $annonce->id ]]]) }}
    {{ Form::token() }}
    <div>
        {{ Form::label('title', 'Titre de l\'annonce') }}
        {{ Form::text('title', $annonce->title, ['class' => 'form-control']) }}
    </div>
    <div>
        {{ Form::label('description', 'Description de l\'annonce', ['class' => 'form-label']) }}
        {{ Form::textarea('description', $annonce->description, ['class' => 'form-control']) }}
    </div>
    <div>
        {{ Form::label('price', 'Prix de l\'annonce', ['class' => 'form-label']) }}
        {{ Form::number('price', $annonce->price, ['class' => 'form-control', 'step' => 'any']) }}
    </div>
    <div>
        {{ Form::label('contact_name', 'Nom du contact de l\'annonce', ['class' => 'form-label']) }}
        {{ Form::text('contact_name', $annonce->contact_name, ['class' => 'form-control']) }}
    </div>
    <div>
        {{ Form::label('contact_email', 'Email du contact de l\'annonce', ['class' => 'form-label']) }}
        {{ Form::text('contact_email', $annonce->contact_email, ['class' => 'form-control']) }}
    </div>
    <div>
        {{ Form::label('contact_phone_number', 'Numéro de téléphone de l\'annonce', ['class' => 'form-label']) }}
        {{ Form::text('contact_phone_number', $annonce->contact_phone_number, ['class' => 'form-control']) }}
    </div>
    <div>
        {{ Form::select('status_id', $annonce_statuses, $annonce->status_id, ['placeholder' => 'Statut de l\'annonce', 'class' => 'form-control']); }}
    </div>
    {{ Form::submit('Enregistrer mes modification', [ 'class' => 'btn btn-info']) }}
{!! Form::close() !!}
@endsection
