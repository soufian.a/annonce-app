@extends('layout')

@section('page_title')
Créer une nouvelle annonce
@endsection

@section('content')
<h2>Créez votre annonce</h2>
{!! Form::open(['route' => ['annonce.store']]) !!}
    {{ Form::token() }}
    <div>
        {{ Form::label('title', 'Titre de l\'annonce') }}
        {{ Form::text('title', '', ['class' => 'form-control']) }}
    </div>
    <div>
        {{ Form::label('description', 'Description de l\'annonce', ['class' => 'form-label']) }}
        {{ Form::textarea('description', '', ['class' => 'form-control']) }}
    </div>
    <div>
        {{ Form::label('price', 'Prix de l\'annonce', ['class' => 'form-label']) }}
        {{ Form::number('price', '', ['class' => 'form-control', 'step' => 'any']) }}
    </div>
    <div>
        {{ Form::label('contact_name', 'Nom du contact de l\'annonce', ['class' => 'form-label']) }}
        {{ Form::text('contact_name', '', ['class' => 'form-control']) }}
    </div>
    <div>
        {{ Form::label('contact_email', 'Email du contact de l\'annonce', ['class' => 'form-label']) }}
        {{ Form::text('contact_email', '', ['class' => 'form-control']) }}
    </div>
    <div>
        {{ Form::label('contact_phone_number', 'Numéro de téléphone de l\'annonce', ['class' => 'form-label']) }}
        {{ Form::text('contact_phone_number', '', ['class' => 'form-control']) }}
    </div>
    <div>
        {{ Form::select('status_id', $annonce_statuses, null, ['placeholder' => 'Statut de l\'annonce', 'class' => 'form-control']); }}
    </div>
    {{ Form::submit('Enregistrer mon annonce') }}
{!! Form::close() !!}
@endsection
