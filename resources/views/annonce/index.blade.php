@extends('layout')

@section('page_title')
    Liste des annonces disponibles
@endsection

@section('content')
<div class="container">
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Id</th>
                <th scope="col">Titre</th>
                <th scope="col">Description</th>
                <th scope="col">Prix</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($annonces as $annonce)
                <tr>
                    <th scope="row">{{ $loop->iteration }}</th>
                    <th>{{ $annonce->id }}</th>
                    <td><a href="{{ route('annonce.show', $annonce->id) }}">{{ $annonce->title }}</a></td>
                    <td>{{ $annonce->description }}</td>
                    <td>{{ $annonce->price }} €</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
