@extends('layout')

@section('page_title')
    L'annonce {{ $annonce->title }}
@endsection

@section('content')
    <div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-light">
        <div class="col-md-5 p-lg-5 mx-auto my-5">
            <h1 class="display-4 fw-normal">{{ $annonce->title }}</h1>
            <p class="lead fw-normal">{{ $annonce->description }}</p>
            <a class="btn btn-outline-secondary" href="#">{{ $annonce->price }}</a>
        </div>
        <div class="product-device shadow-sm d-none d-md-block"></div>
        <div class="product-device product-device-2 shadow-sm d-none d-md-block"></div>
    </div>
    @auth
        @if($user->id === $annonce->user_id)
            <a href="{{ route('annonce.update', $annonce->id) }}" class="btn btn-success">Mettre à jour l'annonce</a>
        @endif
    @endauth
@endsection
