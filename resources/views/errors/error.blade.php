@extends('layout')

@section('page_title')
Une erreur est sûrvenue
@endsection

@section('content')
<div class="container">
    <p>Une erreur de navigation est sûrvenue, en voici le message:<br>
    {{ $message }}</p>
</div>
@endsection
