@extends('layout')

@section('page_title')
Accès interdit
@endsection

@section('content')
<div class="container">
    Vous n'avez pas la permission d'accéder à la ressource demandée.
</div>
@endsection
